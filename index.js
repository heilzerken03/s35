const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")

dotenv.config()

const app = express()
const port = 3001

// change <password>
// npm install dotenv
// .env - can store sensitive information.
// MongoDB Connection
mongoose.connect(`mongodb+srv://kenheilzer:${process.env.MONGODB_PASSWORD}@cluster0.wtjmdvw.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// set as variable for initialization
let db = mongoose.connection

// .on for the event 'error' then arrow function (console.error)
db.on('error', () => console.error("Connection error."))
db.on('open', () => console.log("Connected to MongoDB!"))
// MongoDB Connection END

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
// const taskSchema = new mongoose.Schema({
// 	name: String,
// 	status: {
// 		type: String,
// 		default: 'Pending'
// 	}
// })
// // MongoDB Schemas END

// // MongoDB Model
// // for .model the function should start with upper case
// // for .model the table name should be in singular form
// const Task = mongoose.model('Task', taskSchema)
// // MongoDB Model END

// // ROUTES
// app.post('/tasks', (request, response) => {
// 	Task.findOne({name: request.body.name}, (error, result) => {
// 		if(result != null && result.name == request.body.name) {
// 			return response.send('Duplicate task found!')
// 		}

// 		let newTask = new Task ({
// 			name: request.body.name
// 		})

// 		newTask.save((error, savedTask) => {
// 			if(error){
// 				return console.error(error)
// 			} else {
// 				return response.status(200).send('New task created!')
// 			}
// 		})
// 	})
// })
// // {} - to retrieve all the task requested
// app.get('/tasks', (request, response) => {
// 	Task.find({}, (error, result) => {
// 		if(error) {
// 			return console.log(errors)
// 		}
// 		return response.status(200).json({
// 			data: result
// 		})
// 	})
// })
// // ROUTES END

// app.listen(port, () => console.log(`Server running at localhost: ${port}`))

// ===========================================================
// s35 - ACTIVITY

const signupSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})
const Signup = mongoose.model('Signup', signupSchema)

app.post('/signup', (request, response) => {
	Signup.findOne({name: request.body.name}, (error, result) => {
		if(result != null && result.name == request.body.name) {
			return response.send('Duplicate user found!')
		}

		let newSignup = new Signup ({
			name: request.body.name
		})

		newSignup.save((error, savedSignup) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New user created!')
			}
		})
	})
})

app.listen(port, () => console.log(`Server running at localhost: ${port}`))